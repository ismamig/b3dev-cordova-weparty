// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.5.0/firebase-app.js";
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
  deleteUser,
  updateProfile,
} from "https://www.gstatic.com/firebasejs/9.5.0/firebase-auth.js";
import {
  getFirestore,
  collection,
  addDoc,
  getDocs,
  setDoc,
  doc,
  deleteDoc,
  updateDoc,
  getDoc,
  onSnapshot,
} from "https://www.gstatic.com/firebasejs/9.5.0/firebase-firestore.js";
import "./store.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCabIT5DzAj3EaRpa4C1qFjqMOFIrxUccM",
  authDomain: "weparty-62547.firebaseapp.com",
  projectId: "weparty-62547",
  storageBucket: "weparty-62547.appspot.com",
  messagingSenderId: "398477279105",
  appId: "1:398477279105:web:5171b78c7ed92ef088b280",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth();
const user = auth.currentUser;
const db = getFirestore();

onAuthStateChanged(auth, (user) => {
  if (user) {
    console.log("sign in!");
    store.dispatch("connect", user);
  } else {
    store.dispatch("disconnect");
    console.log("sign out!");
    console.log(store.getters.current_user.value);
  }
});

// sign in
$("#my-login-screen .login-button").on("click", function () {
  var email = $('#my-login-screen [name="email"]').val();
  var password = $('#my-login-screen [name="password"]').val();
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in
      const user = userCredential.user;
      // ...
      onSnapshot(doc(db, "users", auth.currentUser.uid), (doc) => {
        store.dispatch("setUserInfos", doc.data());
        console.log(store.getters.user_infos.value);
      });
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
    });

  app.loginScreen.close("#my-login-screen");

  // Alert username and password
  app.dialog.alert("Connecté!");
});

// AUTH

// sign up
$("#my-login-screen .signup-button").on("click", function () {
  var email = $('#my-login-screen [name="email"]').val();
  var password = $('#my-login-screen [name="password"]').val();
  createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in
      const user = userCredential.user;
      setDoc(doc(db, "users", user.uid), {
        id: user.uid,
        email: user.email,
        displayname: user.displayName,
        phoneNumber: user.phoneNumber,
        desc: "",
        birthDate: "",
        createdDate: user.metadata.creationTime,
      });
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
    });

  app.loginScreen.close("#my-login-screen");

  // Alert username and password
  app.dialog.alert("Compte créé avec succes!");
});

// sign out
$(".open-confirm-signout").on("click", function () {
  app.dialog.confirm("deconnexion ?", function () {
    signOut(auth)
      .then(() => {
        //Sign-out sucessful
        console.log("deco");
      })
      .catch((error) => {
        //An error
        console.log(error);
      });
  });
});

// delete account
$(".open-confirm-delaccount").on("click", function () {
  app.dialog.confirm("Supprimer definitivement le compte ?", function () {
    deleteDoc(doc(db, "users", store.getters.current_user.value.uid))
      .then(() => {
        console.log("deleteDoc!");
        deleteUser(auth.currentUser)
          .then(() => {
            console.log("deleteUser");
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
    app.dialog.alert("Compte supprimé. Vous allez être déconnecté!");
  });
});

// FIRESTORE USERS --

// firestore values in input values
$(document).on("click", ".account-edit-link", function () {
  // const user_infos = onSnapshot(doc(db, "users", auth.currentUser.uid), (doc) => {
  //     console.log("Current data: ", doc.data());
  //     store.dispatch('setUserInfos', doc.data())
  //     console.log(store.getters.user_infos.value)
  // });
});

// UPDATE PROFILE INFOS
$(document).on("click", ".validate-profile", function () {
  var profile_infos = {
    displayName: $("#edit-profile [name=fullname]").val(),
    desc: $('#edit-profile [name="desc"]').val(),
  };
  console.log(profile_infos);

  // $store.dispatch('updateUserInfos', profile_infos)

  updateProfile(auth.currentUser, {
    displayName: profile_infos.displayName,
    description: profile_infos.desc,
  })
    .then(() => {
      updateDoc(doc(db, "users", auth.currentUser.uid), {
        displayname: profile_infos.displayName,
        desc: profile_infos.desc,
      });
      console.log("profile updated!");
      console.log(auth.currentUser);
    })
    .catch((error) => {
      console.log(error);
    });
});
// TODO: Get firebase infos and put it in input value

// ACCOUNT INFOS
// TODO: update firebase account infos
$(document).on("click", ".validate-account", function () {
  var account_infos = {
    type: $("#input-account-edit").attr("type"),
    value: $("#input-account-edit").val(),
  };
  if (account_infos.value != "") {
    if (account_infos.type == "email") {
      // Update email
    } else if (account_infos.type == "password") {
      // Update password
    } else if (account_infos.type == "date") {
      updateDoc(doc(db, "users", auth.currentUser.uid), {
        birthDate: account_infos.value,
      });
    } else if (account_infos.type == "tel") {
      updateProfile(auth.currentUser, {
        phoneNumber: account_infos.value,
      })
        .then(() => {
          updateDoc(doc(db, "users", auth.currentUser.uid), {
            phoneNumber: account_infos.value,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

    // } else if (account_infos.type == '')
    console.log(account_infos)
})




// New Event Creation
$(document).on('click', '#newEventCreate', function () {

  var title = $('#new-event [name="title-event"]').val();
  var description = $('#new-event [name="desc-event"]').val();
  var date = $('#new-event [name="date-event"]').val();
  var categorie = $('#new-event [name="cat-event"]').val();
   
  setDoc(doc(db, "event", ""+title+""), {
    title: title,
    description: description,
    date: date,
    categorie: categorie
    // author: author

  }).then((promise) => {
    console.log("Évenement créer / modifier");
  }).catch(error => {
    console.error('Erreur : ', error);
  })
});


//DISPLAY FIRESTORE DATA

const querySnapshot = await getDocs(collection(db, "event"));
querySnapshot.forEach((doc) => {
  // doc.data() is never undefined for query doc snapshots
  // console.log(doc.id, " => ", doc.data());
  store.dispatch('getEvent', doc.data());
});

var $$ = Dom7;

var routes = [
  {
    path: "/",
    url: "./index.html",
  },
  {
    path: "/about/",
    url: "./pages/about.html",
  },
  {
    path: "/form/",
    url: "./pages/form.html",
  },
  {
    path: "/catalog/",
    componentUrl: "./pages/catalog.html",
  },
  {
    path: "/product/:id/",
    componentUrl: "./pages/product.html",
  },
  {
    path: "/settings/",
    url: "./pages/settings.html",
  },
  {
    path: "/profile/",
    componentUrl: "./pages/profile.html",
  },
  {
    path: "/profile/edit/",
    componentUrl: "./pages/editprofile.html",
  },
  {
    path: "/account/edit/:id/",
    componentUrl: "./pages/editaccount.html",
  },
  {
    path: "/calendar/",
    componentUrl: "./pages/calendar.html",
    on: {
      pageAfterIn: function (e, page) {
        var router = this;
        var app = router.app;
        var onNewEvent = true;


        $$("#add-event").on("click", function () {
          if (onNewEvent) {
            $$("#add-event").transform("rotate(45deg)");
            $$("#add-event").css({ "background-color": "white" });
            $$("#add-event-icon").css({ color: "black" });
            $$("#new-event").css({ display: "block" });
            $$("#new-event").css({ height: "auto" });
            $$("#event-group").css({ "background-color": "#007aff" });
            onNewEvent = false;
          } else {
            $$("#add-event").transform("rotate(0deg)");
            $$("#add-event").css({ "background-color": "#007aff" });
            $$("#add-event-icon").css({ color: "white" });
            $$("#new-event").css({ display: "none" });
            $$("#new-event").css({ height: "0" });
            $$("#event-group").css({ "background-color": "white" });
            onNewEvent = true;
          }
          // alert('test');
        });
        
      },
      pageInit: function (e, page) {
        var router = this;
        var app = router.app; //APP correspond aux composants

        calendarDateTime = app.calendar.create({
          inputEl: "#demo-calendar-date-time",
          timePicker: true,
          dateFormat: {
            month: "numeric",
            day: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
          },
        });
      },
    },
  },

  {
    path: "/dynamic-route/blog/:blogId/post/:postId/",
    componentUrl: "./pages/dynamic-route.html",
  },
  {
    path: "/request-and-load/user/:userId/",
    async: function ({ router, to, resolve }) {
      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = to.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: "Vladimir",
          lastName: "Kharlampidi",
          about: "Hello, i am creator of Framework7! Hope you like it!",
          links: [
            {
              title: "Framework7 Website",
              url: "http://framework7.io",
            },
            {
              title: "Framework7 Forum",
              url: "http://forum.framework7.io",
            },
          ],
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            componentUrl: "./pages/request-and-load.html",
          },
          {
            props: {
              user: user,
            },
          }
        );
      }, 1000);
    },
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: "(.*)",
    url: "./pages/404.html",
  },
];

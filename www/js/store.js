

var createStore = Framework7.createStore;
const store = createStore({
  state: {
    current_user: {},
    user_infos: {},
    edit_profile: [
      {
        id: '1',
        title: 'Courrier électronique',
        placeholder: "foo@email.com",
        type: 'email',
        name: 'mail-input'
      },
      {
        id: '2',
        title: 'Mot de passe',
        placeholder: "****************",
        type: 'password',
        name: 'password-input'
      },
      {
        id: '3',
        title: 'Numéro de télephone',
        placeholder: "0XXXXXXXXX",
        type: 'tel',
        name: 'tel-input'
      },
      {
        id: '4',
        title: 'Date de naissance',
        placeholder: "XX/XX/XX",
        type: 'date',
        name: 'date-input'
      },
    ],
    input_account_edit: "",
    products: [
      {
        id: '1',
        title: 'Apple iPhone 8',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi tempora similique reiciendis, error nesciunt vero, blanditiis pariatur dolor, minima sed sapiente rerum, dolorem corrupti hic modi praesentium unde saepe perspiciatis.'
      },
      {
        id: '2',
        title: 'Apple iPhone 8 Plus',
        description: 'Velit odit autem modi saepe ratione totam minus, aperiam, labore quia provident temporibus quasi est ut aliquid blanditiis beatae suscipit odio vel! Nostrum porro sunt sint eveniet maiores, dolorem itaque!'
      },
      {
        id: '3',
        title: 'Apple iPhone X',
        description: 'Expedita sequi perferendis quod illum pariatur aliquam, alias laboriosam! Vero blanditiis placeat, mollitia necessitatibus reprehenderit. Labore dolores amet quos, accusamus earum asperiores officiis assumenda optio architecto quia neque, quae eum.'
      },
    ],
    event_info: []
  },
  getters: {
    products({ state }) {
      return state.products;
    },
    edit_profile({ state }) {
      return state.edit_profile;
    },
    current_user({ state }) {
      return state.current_user;
    },
    account_input({ state }) {
      return state.input_account_edit
    },
    user_infos({ state }) {
      return state.user_infos
      return state.currentUser;
    },
    event_info({ state }) {
      return state.event_info;
    }
  },
  actions: {
    addProduct({ state }, product) {
      state.products = [...state.products, product];
    },
    connect({ state }, user) {
      state.currentUser = user;
    },
    disconnect({ state }) {
      state.currentUser = []
    },
    updateUserInfos({ state }, profile_infos) {
      state.currentUser.displayname = profile_infos.displayname
      state.currentUser.description = profile_infos.desc
    },
    updateUserAccount({ state }, account_infos) {
      state.input_account_edit = account_infos
    },
    setUserInfos({ state }, data) {
      state.user_infos = data
    getEvent({ state }, event) {
      state.event_info = [...state.event_info, event]
    }
  },
})